﻿namespace pitech.visual
{
    partial class Ejercicio1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRecibirDatos = new System.Windows.Forms.Button();
            this.btnGenerarCSV = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnRecibirDatos
            // 
            this.btnRecibirDatos.Location = new System.Drawing.Point(430, 141);
            this.btnRecibirDatos.Name = "btnRecibirDatos";
            this.btnRecibirDatos.Size = new System.Drawing.Size(102, 22);
            this.btnRecibirDatos.TabIndex = 0;
            this.btnRecibirDatos.Text = "Recibir Datos";
            this.btnRecibirDatos.UseVisualStyleBackColor = true;
            this.btnRecibirDatos.Click += new System.EventHandler(this.BtnRecibirDatos_Click);
            // 
            // btnGenerarCSV
            // 
            this.btnGenerarCSV.Location = new System.Drawing.Point(241, 210);
            this.btnGenerarCSV.Name = "btnGenerarCSV";
            this.btnGenerarCSV.Size = new System.Drawing.Size(291, 53);
            this.btnGenerarCSV.TabIndex = 1;
            this.btnGenerarCSV.Text = "Generar CSV";
            this.btnGenerarCSV.UseVisualStyleBackColor = true;
            this.btnGenerarCSV.Click += new System.EventHandler(this.BtnGenerarCSV_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(341, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ejercicio 1";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(241, 141);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(183, 22);
            this.txtFilePath.TabIndex = 3;
            // 
            // Ejercicio1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGenerarCSV);
            this.Controls.Add(this.btnRecibirDatos);
            this.Name = "Ejercicio1";
            this.Text = "ejercicio1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRecibirDatos;
        private System.Windows.Forms.Button btnGenerarCSV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFilePath;
    }
}