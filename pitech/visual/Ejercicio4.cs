﻿using Newtonsoft.Json;
using pitech.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pitech.visual
{
    //comment to update..
    public partial class Ejercicio4 : Form
    {
        private string result = "";

        // metodo para convertir numeros en letras; toText method by https://social.msdn.microsoft.com/Forums/es-ES/a7854b67-3847-4673-8277-bdbf07fa8522/convertir-de-numeros-a-letras?forum=vcses
        private string toText(double value)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + toText(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + toText(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " + toText(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + toText(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " + toText(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + toText(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + toText(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + toText(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";
                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000) * 1000000);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            else
            {
                Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }
            return Num2Text;

        }

        // metodo para obtener opciones
        private List<string> obtenerOpciones(string result)
        {
            List<string> opciones = new List<string>();
            try
            {
                List<string> resultadoSeparado = new List<string>();

                //convertir resultado en lista de strings
                resultadoSeparado.AddRange(result.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));

                Random random = new Random();
                int contador = 0;

                foreach (var resultado in resultadoSeparado)
                {
                    List<string> subString = new List<string>();

                    if (resultado == "BILLONES" || resultado == "MILLONES" || resultado == "BILLON" || resultado == "MILLON")
                    {
                        for (int i = contador; i < resultadoSeparado.IndexOf(resultado); i++)
                        {
                            subString.Add(resultadoSeparado[i]);
                            contador = i + 1;
                        }
                        if (subString.Count == 1)
                        {
                            opciones.Add(subString[0]);
                        }
                        else if (subString.Count > 1)
                        {
                            opciones.Add(String.Join(" ", subString.ToArray()));
                        }
                        subString.Clear();
                    }
                    else if (resultado == "MIL")
                    {
                        //numero antes de mil
                        for (int i = contador; i < resultadoSeparado.IndexOf(resultado); i++)
                        {
                            subString.Add(resultadoSeparado[i]);
                            contador = i + 1;
                        }
                        if (subString.Count == 1)
                        {
                            opciones.Add(subString[0]);
                        }
                        else if (subString.Count > 1)
                        {
                            opciones.Add(String.Join(" ", subString.ToArray()));
                        }
                        subString.Clear();

                        // mil
                        opciones.Add(resultadoSeparado[resultadoSeparado.IndexOf(resultado)]);

                        // numero despues de mil
                        for (int i = (resultadoSeparado.IndexOf(resultado)) + 1; i < resultadoSeparado.Count; i++)
                        {
                            subString.Add(resultadoSeparado[i]);
                            contador = i + 1;
                        }
                        if (subString.Count == 1)
                        {
                            opciones.Add(subString[0]);
                        }
                        else if (subString.Count > 1)
                        {
                            opciones.Add(String.Join(" ", subString.ToArray()));
                        }
                        subString.Clear();

                    }
                }
                //agregar opciones random
                while (opciones.Count <= 6)
                {
                    opciones.Add(toText(random.Next(100)));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            return opciones;
        }

        public Ejercicio4()
        {
            InitializeComponent();
            try
            {

                ModelEjercicio4 ejercicio4 = new ModelEjercicio4();
                Random random = new Random();
                ejercicio4.Options = new List<string>();
                ejercicio4.Instruction = "Escribe el numero siguiente presionando los botones en el orden correcto.";
                ejercicio4.Problem = random.Next(1000, 10000);
                ejercicio4.Result = result = toText(ejercicio4.Problem);

                //obtener opciones
                List<string> opciones = obtenerOpciones(ejercicio4.Result);

                //desordenar opciones
                int randomIndex = 0;
                while (opciones.Count > 0)
                {
                    randomIndex = random.Next(0, opciones.Count);
                    ejercicio4.Options.Add(opciones[randomIndex]);
                    opciones.RemoveAt(randomIndex);
                }


                //agregar componentes a la vista
                lblInstruccion.Text = ejercicio4.Instruction;
                lblProblema.Text = ejercicio4.Problem.ToString("N0");

                //agregar opciones a la vista
                for (int i = 0; i < ejercicio4.Options.Count; i++)
                {
                    var btns = Controls.Find("btnOpcion" + i, true);
                    if (btns.Length > 0)
                    {
                        var btn = (Button)btns[0];
                        btn.Text = ejercicio4.Options[i];
                    }

                }

                //generar json
                string jsonResult = JsonConvert.SerializeObject(ejercicio4);
                File.WriteAllText(@"ejercicio4.json", jsonResult);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void BtnOpcion0_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text.Contains(btnOpcion0.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(btnOpcion0.Text, "");
            }
            else if (txtResultado.Text.Contains(" " + btnOpcion0.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(" " + btnOpcion0.Text, "");
            }
            else if (txtResultado.Text.Length == 0)
            {
                txtResultado.Text += btnOpcion0.Text;
            }
            else
            {
                txtResultado.Text += " " + btnOpcion0.Text;
            }
        }

        private void BtnOpcion1_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text.Contains(btnOpcion1.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(btnOpcion1.Text, "");
            }
            else if (txtResultado.Text.Contains(" " + btnOpcion1.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(" " + btnOpcion1.Text, "");
            }
            else if (txtResultado.Text.Length == 0)
            {
                txtResultado.Text += btnOpcion1.Text;
            }
            else
            {
                txtResultado.Text += " " + btnOpcion1.Text;
            }
        }

        private void BtnOpcion2_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text.Contains(btnOpcion2.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(btnOpcion2.Text, "");
            }
            else if (txtResultado.Text.Contains(" " + btnOpcion2.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(" " + btnOpcion2.Text, "");
            }
            else if (txtResultado.Text.Length == 0)
            {
                txtResultado.Text += btnOpcion2.Text;
            }
            else
            {
                txtResultado.Text += " " + btnOpcion2.Text;
            }
        }

        private void BtnOpcion3_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text.Contains(btnOpcion3.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(btnOpcion3.Text, "");
            }
            else if (txtResultado.Text.Contains(" " + btnOpcion3.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(" " + btnOpcion3.Text, "");
            }
            else if (txtResultado.Text.Length == 0)
            {
                txtResultado.Text += btnOpcion3.Text;
            }
            else
            {
                txtResultado.Text += " " + btnOpcion3.Text;
            }
        }

        private void BtnOpcion4_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text.Contains(btnOpcion4.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(btnOpcion4.Text, "");
            }
            else if (txtResultado.Text.Contains(" " + btnOpcion4.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(" " + btnOpcion4.Text, "");
            }
            else if (txtResultado.Text.Length == 0)
            {
                txtResultado.Text += btnOpcion4.Text;
            }
            else
            {
                txtResultado.Text += " " + btnOpcion4.Text;
            }
        }

        private void BtnOpcion5_Click(object sender, EventArgs e)
        {
            if (txtResultado.Text.Contains(btnOpcion5.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(btnOpcion5.Text, "");
            }
            else if (txtResultado.Text.Contains(" " + btnOpcion5.Text))
            {
                txtResultado.Text = txtResultado.Text.Replace(" " + btnOpcion5.Text, "");
            }
            else if (txtResultado.Text.Length == 0)
            {
                txtResultado.Text += btnOpcion5.Text;
            }
            else
            {
                txtResultado.Text += " " + btnOpcion5.Text;
            }
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if(txtResultado.Text == result)
            {
                MessageBox.Show("Correcto");
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrecto");
            }
        }
    }
}
