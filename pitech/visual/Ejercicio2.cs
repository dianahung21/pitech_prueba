﻿using Newtonsoft.Json;
using pitech.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pitech.visual
{
    //comment to update..
    public partial class Ejercicio2 : Form
    {
        private string result = "";

        public Ejercicio2()
        {
            InitializeComponent();

            try
            {
                ModelEjercicio2 ejercicio2 = new ModelEjercicio2();
                Random random = new Random();
                ejercicio2.Problem = new List<int>();
                ejercicio2.Options = new List<int>();
                ejercicio2.Instruction = "Selecciona el resultado de la siguiente suma";
                ejercicio2.Problem.Add(random.Next(1000, 1000001));
                ejercicio2.Problem.Add(random.Next(1000, 1000001));

                //respuesta correcta
                result = (ejercicio2.Problem[0] + ejercicio2.Problem[1]).ToString("N0");

                //agregar opciones
                List<int> opciones = new List<int>();
                opciones.Add(ejercicio2.Problem[0] + ejercicio2.Problem[1]);
                opciones.Add(Math.Abs(ejercicio2.Problem[0] - ejercicio2.Problem[1]));
                opciones.Add(ejercicio2.Problem[0] + (ejercicio2.Problem[1] / 2));
                opciones.Add((ejercicio2.Problem[0] + ejercicio2.Problem[1]) / 2);

                //desordenar opciones
                int randomIndex = 0;
                while (opciones.Count > 0)
                {
                    randomIndex = random.Next(0, opciones.Count);
                    ejercicio2.Options.Add(opciones[randomIndex]);
                    opciones.RemoveAt(randomIndex);
                }

                // agregar informacion a la vista
                lblInstruccion.Text = ejercicio2.Instruction;
                lblPrimerNumero.Text = ejercicio2.Problem[0].ToString("N0");
                lblSegundoNumero.Text = ejercicio2.Problem[1].ToString("N0");
                for (int i = 0; i < ejercicio2.Options.Count; i++)
                {
                    var btns = Controls.Find("btnOpcion" + i, true);
                    if (btns.Length > 0)
                    {
                        var btn = (Button)btns[0];
                        btn.Text = ejercicio2.Options[i].ToString("N0");
                    }

                }

                //generar json
                string jsonResult = JsonConvert.SerializeObject(ejercicio2);
                File.WriteAllText(@"ejercicio2.json", jsonResult);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }            
                       
        }


        private void BtnOpcion0_Click(object sender, EventArgs e)
        {
            if (btnOpcion0.Text == result)
            {
                MessageBox.Show("Correcto");
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrecto");
            }
        }

        private void BtnOpcion1_Click(object sender, EventArgs e)
        {
            if (btnOpcion1.Text == result)
            {
                MessageBox.Show("Correcto");
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrecto");
            }
        }

        private void BtnOpcion2_Click(object sender, EventArgs e)
        {
            if (btnOpcion2.Text == result)
            {
                MessageBox.Show("Correcto");
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrecto");
            }
        }

        private void BtnOpcion3_Click(object sender, EventArgs e)
        {
            if (btnOpcion3.Text == result)
            {
                MessageBox.Show("Correcto");
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrecto");
            }
        }
    }
}
