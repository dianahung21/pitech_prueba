﻿namespace pitech.visual
{
    partial class Ejercicio4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblInstruccion = new System.Windows.Forms.Label();
            this.lblProblema = new System.Windows.Forms.Label();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.btnOpcion0 = new System.Windows.Forms.Button();
            this.btnOpcion1 = new System.Windows.Forms.Button();
            this.btnOpcion2 = new System.Windows.Forms.Button();
            this.btnOpcion3 = new System.Windows.Forms.Button();
            this.btnOpcion4 = new System.Windows.Forms.Button();
            this.btnOpcion5 = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(336, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ejercicio 4";
            // 
            // lblInstruccion
            // 
            this.lblInstruccion.AutoSize = true;
            this.lblInstruccion.Location = new System.Drawing.Point(153, 80);
            this.lblInstruccion.Name = "lblInstruccion";
            this.lblInstruccion.Size = new System.Drawing.Size(76, 17);
            this.lblInstruccion.TabIndex = 1;
            this.lblInstruccion.Text = "Instruccion";
            // 
            // lblProblema
            // 
            this.lblProblema.AutoSize = true;
            this.lblProblema.Location = new System.Drawing.Point(336, 129);
            this.lblProblema.Name = "lblProblema";
            this.lblProblema.Size = new System.Drawing.Size(68, 17);
            this.lblProblema.TabIndex = 2;
            this.lblProblema.Text = "Problema";
            // 
            // txtResultado
            // 
            this.txtResultado.Location = new System.Drawing.Point(130, 173);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.Size = new System.Drawing.Size(423, 22);
            this.txtResultado.TabIndex = 3;
            // 
            // btnOpcion0
            // 
            this.btnOpcion0.Location = new System.Drawing.Point(56, 231);
            this.btnOpcion0.Name = "btnOpcion0";
            this.btnOpcion0.Size = new System.Drawing.Size(162, 70);
            this.btnOpcion0.TabIndex = 4;
            this.btnOpcion0.Text = "Opcion 1";
            this.btnOpcion0.UseVisualStyleBackColor = true;
            this.btnOpcion0.Click += new System.EventHandler(this.BtnOpcion0_Click);
            // 
            // btnOpcion1
            // 
            this.btnOpcion1.Location = new System.Drawing.Point(298, 231);
            this.btnOpcion1.Name = "btnOpcion1";
            this.btnOpcion1.Size = new System.Drawing.Size(162, 70);
            this.btnOpcion1.TabIndex = 5;
            this.btnOpcion1.Text = "Opcion 2";
            this.btnOpcion1.UseVisualStyleBackColor = true;
            this.btnOpcion1.Click += new System.EventHandler(this.BtnOpcion1_Click);
            // 
            // btnOpcion2
            // 
            this.btnOpcion2.Location = new System.Drawing.Point(539, 231);
            this.btnOpcion2.Name = "btnOpcion2";
            this.btnOpcion2.Size = new System.Drawing.Size(161, 70);
            this.btnOpcion2.TabIndex = 6;
            this.btnOpcion2.Text = "Opcion 3";
            this.btnOpcion2.UseVisualStyleBackColor = true;
            this.btnOpcion2.Click += new System.EventHandler(this.BtnOpcion2_Click);
            // 
            // btnOpcion3
            // 
            this.btnOpcion3.Location = new System.Drawing.Point(56, 322);
            this.btnOpcion3.Name = "btnOpcion3";
            this.btnOpcion3.Size = new System.Drawing.Size(162, 65);
            this.btnOpcion3.TabIndex = 7;
            this.btnOpcion3.Text = "Opcion 4";
            this.btnOpcion3.UseVisualStyleBackColor = true;
            this.btnOpcion3.Click += new System.EventHandler(this.BtnOpcion3_Click);
            // 
            // btnOpcion4
            // 
            this.btnOpcion4.Location = new System.Drawing.Point(298, 322);
            this.btnOpcion4.Name = "btnOpcion4";
            this.btnOpcion4.Size = new System.Drawing.Size(162, 65);
            this.btnOpcion4.TabIndex = 8;
            this.btnOpcion4.Text = "Opcion 5";
            this.btnOpcion4.UseVisualStyleBackColor = true;
            this.btnOpcion4.Click += new System.EventHandler(this.BtnOpcion4_Click);
            // 
            // btnOpcion5
            // 
            this.btnOpcion5.Location = new System.Drawing.Point(539, 322);
            this.btnOpcion5.Name = "btnOpcion5";
            this.btnOpcion5.Size = new System.Drawing.Size(161, 65);
            this.btnOpcion5.TabIndex = 9;
            this.btnOpcion5.Text = "Opcion 6";
            this.btnOpcion5.UseVisualStyleBackColor = true;
            this.btnOpcion5.Click += new System.EventHandler(this.BtnOpcion5_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(559, 173);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 10;
            this.btnOk.Text = "ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // Ejercicio4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnOpcion5);
            this.Controls.Add(this.btnOpcion4);
            this.Controls.Add(this.btnOpcion3);
            this.Controls.Add(this.btnOpcion2);
            this.Controls.Add(this.btnOpcion1);
            this.Controls.Add(this.btnOpcion0);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.lblProblema);
            this.Controls.Add(this.lblInstruccion);
            this.Controls.Add(this.label1);
            this.Name = "Ejercicio4";
            this.Text = "Ejercicio4";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblInstruccion;
        private System.Windows.Forms.Label lblProblema;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.Button btnOpcion0;
        private System.Windows.Forms.Button btnOpcion1;
        private System.Windows.Forms.Button btnOpcion2;
        private System.Windows.Forms.Button btnOpcion3;
        private System.Windows.Forms.Button btnOpcion4;
        private System.Windows.Forms.Button btnOpcion5;
        private System.Windows.Forms.Button btnOk;
    }
}