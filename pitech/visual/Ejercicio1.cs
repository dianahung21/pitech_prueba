﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace pitech.visual
{
    //comment to update..
    public partial class Ejercicio1 : Form
    {
        private List<string> cedulas = new List<string>();
        private IDictionary<string, string> data = new Dictionary<string, string>();
        private IDictionary<string, string> dataOrdenados = new Dictionary<string, string>();
        public Ejercicio1()
        {
            InitializeComponent();

            txtFilePath.Text = "Ejercicio1 (3) (1).csv";
        }
        private void BtnRecibirDatos_Click(object sender, EventArgs e)
        {
            try
            {
                using (var datos = new StreamReader($@"{txtFilePath.Text}"))
                {

                    while (!datos.EndOfStream)
                    {
                        var linea = datos.ReadLine();
                        var valores = linea.Split(',');

                        cedulas.Add(valores[1]);
                        data.Add(valores[1], valores[0]);
                    }
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BtnGenerarCSV_Click(object sender, EventArgs e)
        {
            try
            {
                //ordenar las cedulas
                List<string> cedulasOrdenadas = new List<string>();
                foreach (var cedula in cedulas)
                {
                    string numero = cedula.Replace("-", "").ToString();
                    char[] charArray = numero.ToCharArray();
                    Array.Reverse(charArray);
                    string revNumero = new string(charArray);
                    cedulasOrdenadas.Add(revNumero);
                }
                cedulasOrdenadas.Sort();


                // volver a convertir las cedulas en formato original y organizar los datos con su nombre
                for (int i = 0; i < cedulasOrdenadas.Count; i++)
                {
                    string numero = cedulasOrdenadas[i].ToString();
                    char[] charArray = numero.ToCharArray();
                    Array.Reverse(charArray);
                    string revNumero = new string(charArray);
                    revNumero = revNumero.Insert(3, "-");
                    revNumero = revNumero.Insert(charArray.Count(), "-");
                    cedulasOrdenadas[i] = revNumero;

                    if (data.ContainsKey(cedulasOrdenadas[i]))
                    {
                        dataOrdenados.Add(data[cedulasOrdenadas[i]], cedulasOrdenadas[i]);
                    }
                }

                //generar csv
                using (TextWriter sw = new StreamWriter(@"Ejercicio1.csv"))
                {
                    foreach (var item in dataOrdenados)
                    {
                        sw.WriteLine("{0},{1}", item.Key, item.Value);
                    }
                }

                MessageBox.Show("CSV generado correctamente.");
                
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
    }
}
