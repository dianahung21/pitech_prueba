﻿using Newtonsoft.Json;
using pitech.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pitech.visual
{
    //comment to update..
    public partial class Ejercicio3 : Form
    {
        private decimal result = 0;
        public Ejercicio3()
        {
            InitializeComponent();

            try
            {
                ModelEjercicio3 ejercicio3 = new ModelEjercicio3();

                //generar random datos
                Random random = new Random();
                decimal porcentaje = random.Next(100);
                decimal cantidad = random.Next(10000);

                ejercicio3.Options = new List<decimal>();
                ejercicio3.Instruction = "Completa correctamente la oración arrastrando al espacio en blanco la cantidad que corresponda.";
                ejercicio3.Problem = $"Aumentar en un {porcentaje}% la cantidad de {cantidad}, resulta en:";

                // respuesta correcta
                result = (((porcentaje / 100) + 1) * cantidad);

                // generar opciones
                List<decimal> opciones = new List<decimal>();
                opciones.Add(result);
                opciones.Add((porcentaje / 100 * cantidad));
                opciones.Add(porcentaje * cantidad);
                opciones.Add((porcentaje + 100) * cantidad);

                //desordenar opciones
                int randomIndex = 0;
                while (opciones.Count > 0)
                {
                    randomIndex = random.Next(0, opciones.Count);
                    ejercicio3.Options.Add(opciones[randomIndex]);
                    opciones.RemoveAt(randomIndex);
                }

                //agregar informacion a la vista
                lblInstruccion.Text = ejercicio3.Instruction;
                lblProblema.Text = ejercicio3.Problem;
                for (int i = 0; i < ejercicio3.Options.Count; i++)
                {
                    var btns = Controls.Find("btnOpcion" + i, true);
                    if (btns.Length > 0)
                    {
                        var btn = (Button)btns[0];
                        btn.Text = ejercicio3.Options[i].ToString("N");
                    }
                }

                //generar json
                string jsonResult = JsonConvert.SerializeObject(ejercicio3);
                File.WriteAllText(@"ejercicio3.json", jsonResult);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }

        private void BtnOpcion0_Click(object sender, EventArgs e)
        {
            txtResultado.Text = btnOpcion0.Text;
            if(Convert.ToDecimal(txtResultado.Text) == result)
            {
                MessageBox.Show("Correcto");
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrecto");
            }
        }

        private void BtnOpcion1_Click(object sender, EventArgs e)
        {
            txtResultado.Text = btnOpcion1.Text;
            if (Convert.ToDecimal(txtResultado.Text) == result)
            {
                MessageBox.Show("Correcto");
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrecto");
            }
        }

        private void BtnOpcion2_Click(object sender, EventArgs e)
        {
            txtResultado.Text = btnOpcion2.Text;
            if (Convert.ToDecimal(txtResultado.Text) == result)
            {
                MessageBox.Show("Correcto");
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrecto");
            }
        }

        private void BtnOpcion3_Click(object sender, EventArgs e)
        {
            txtResultado.Text = btnOpcion3.Text;
            if (Convert.ToDecimal(txtResultado.Text) == result)
            {
                MessageBox.Show("Correcto");
                Menu menu = new Menu();
                menu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Incorrecto!");
            }
        }
    }
}
