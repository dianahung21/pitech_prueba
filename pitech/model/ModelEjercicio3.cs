﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pitech.model
{
    //comment to update..
    class ModelEjercicio3
    {
        public string Instruction { get; set; }
        public string Problem { get; set; }
        public List<decimal> Options { get; set; }
        public int Result { get; set; } = 0;
    }
}
